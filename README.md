# engulf.sh 

FORKED FROM <https://github.com/shisui98/engulf.sh>

A simple script to "swallow" the terminal, especially while launching zathura. What's so special?
It can use fzf/ranger/thunar too! No more hunting for the right file inside zathura!
It depends on [xdo](https://github.com/baskerville/xdo) to show/hide the window.

This fork is focused on turning this into a tool that can be installed and used on a desktop system.
This includes features such as a `.desktop` file that allows application launchers to detect and make use of engulf.

## TODO

- add an install script
    - would an `install.sh` be best, or would packaging best be done on a per-system basis?
- increase feature set to work with other minimal programs such as mpv
- add other options such as `--help` to make use easier

## usage

With fzf:
```
./engulf.sh zathura fzf 
```

You can open files directly as well:
```
./engulf.sh zathura FILENAME
```
